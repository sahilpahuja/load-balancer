package server;

import java.net.Socket;
import java.util.UUID;

public class Request {
    private final Socket socket;
    private final String route;
    private final long arrivalTime;
    private final java.util.UUID UUID;
    public Request(Socket socket, String route, long arrivalTime, UUID UUID) {
        this.socket = socket;
        this.route = route;
        this.arrivalTime = arrivalTime;
        this.UUID = UUID;
    }

    public Socket getSocket() {
        return socket;
    }

    public String getRoute() {
        return route;
    }

    public long getArrivalTime() {
        return arrivalTime;
    }

    public UUID getUUID() {
        return UUID;
    }

    @Override
    public String toString() {
        return this.UUID.toString()+":"+this.route;
    }
}
