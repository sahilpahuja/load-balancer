package server;

import http.HTTPPacket;
import http.StatusCodes;

public class Controller {
    public String hello(String UUID) {
        return new HTTPPacket(StatusCodes.OK, "hello-world", UUID).toString();
    }
    public String serverError(String UUID) {
        return new HTTPPacket(StatusCodes.INTERNAL_SERVER_ERROR, "Internal Server Error", UUID).toString();
    }
}
