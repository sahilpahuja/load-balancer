package server.load_balancer;

import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.SynchronousQueue;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import http.HTTPPacket;
import http.StatusCodes;
import org.json.simple.parser.ParseException;
import server.Request;
import server.Server;

public class LoadBalancer {
    /**
     * Maintains the order of the incoming request
     */
    SynchronousQueue<UUID> queue;
    /**
     * Mapping of the queued incoming request wth it's content
     */
    private final Map<UUID, Request> requests;
    /**
     * Socket used to communicate with servers
     */
    private final DatagramSocket serverCommunicator;
    /**
     * Address of spawn able servers
     */
    private final InetAddress serverAddress;
    private int activeNoOfWorkers;
    /**
     * Workers mapped with port
     */
    private final HashMap<Integer, Server> activeWorkers;

    /**
     * Numerics used in the implementation of weighted round-robin
     */
    private int lastServerPort;
    private int lastRequest;

    /**
     * Config Variables
     */
    private final int MIN_NO_OF_WORKERS;
    private final int MAX_NO_OF_WORKERS;
    private final int MAX_SCALE_TIME_PER_REQUEST;

    private static final Logger LOGGER = Logger.getLogger( LoadBalancer.class.getName() );

    /**
     * @param minNoOfWorkers which will be started initially
     * @param maxNoOfWorkers which can run actively
     * @param maxScaleTimePerRequest used to scale server based on threshold
     * @throws IOException
     */
    LoadBalancer(
            int minNoOfWorkers,
            int maxNoOfWorkers,
            int maxScaleTimePerRequest
    ) throws IOException {
        // Setting Config
        this.MIN_NO_OF_WORKERS = minNoOfWorkers;
        this.MAX_NO_OF_WORKERS = maxNoOfWorkers;
        this.MAX_SCALE_TIME_PER_REQUEST = maxScaleTimePerRequest;

        initLogger();

        activeWorkers = new HashMap<>();
        queue = new SynchronousQueue<>();
        this.requests = new HashMap<>();
        serverCommunicator = new DatagramSocket(6000);
        this.serverAddress = InetAddress.getLocalHost();

        startLoadBalancer();
        LOGGER.info("Started Load Balancer");
    }

    /**
     * Function to log create the log files and init the logger
     * balancer.log will log {@link Level#ALL}
     * error.log will log {@link Level#SEVERE} and {@link Level#WARNING}
     * @throws IOException
     */
    private void initLogger() throws IOException {
        FileHandler logHandler  = new FileHandler("./balancer.log", true);
        FileHandler errLogHandler  = new FileHandler("./error.log", true);

        logHandler.setFormatter(new SimpleFormatter());
        errLogHandler.setFormatter(new SimpleFormatter());

        errLogHandler.setLevel(Level.SEVERE);
        errLogHandler.setLevel(Level.WARNING);
        logHandler.setLevel(Level.ALL);

        LOGGER.addHandler(errLogHandler);
        LOGGER.addHandler(logHandler);
    }

    /**
     * Starts accepting client request
     * and starts listening for server responses
     */
    private void startLoadBalancer() {
        new RequestConsumer().start();
        try {
            startListenForServers();
        } catch (SocketException e) {
            LOGGER.severe(e.toString());
        }
        createMinNoOfWorkers(this.MIN_NO_OF_WORKERS);
    }

    /**
     * starts listening for server responses
     * @throws SocketException
     */
    private void startListenForServers() throws SocketException {
        new ServerInputReader(this.serverCommunicator).start();
    }

    /**
     * Assigns the request to on of the available servers.
     * Assigning will be done according to the weighted round-robin algorithm
     * @param request request by client
     * @throws IOException
     */
    private void assignToServer(Request request) throws IOException {
        if (this.lastServerPort == 0) {
            this.lastServerPort = 6001;
        }
        Server server = null;

        int activeWorkerNo = (this.lastServerPort - 6000);

        // Assigning algorithm
        if (!(
                activeWorkerNo <= activeNoOfWorkers
                && this.lastRequest < (server = this.activeWorkers.get(this.lastServerPort)).getWeightCapacity()
        )) {
            this.lastRequest = 0;
            // Round-robin on server ports (assuming all servers will run in a range)
            if(activeWorkerNo < activeNoOfWorkers) {
                ++this.lastServerPort;
            } else {
                this.lastServerPort = 6001;
            }

        }
        ++lastRequest;

        System.out.println("Request No: " + lastRequest + " Port No: " + this.lastServerPort);
        sendToServer(this.lastServerPort, request.toString());
    }

    /**
     * Forwards the request to the specified server
     * @param port server port
     * @param request by client
     * @throws IOException
     */
    private void sendToServer(int port, String request) throws IOException {
        byte[] buffer = request.getBytes();
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length, this.serverAddress, port);
        this.serverCommunicator.send(packet);
    }

    /**
     * @param noOfServers
     * no of servers which will be started initially
     */
    private void createMinNoOfWorkers(int noOfServers) {
        int i=this.activeNoOfWorkers+1;
        for (; i<=this.activeNoOfWorkers+noOfServers; ++i) {
            int portToStart = 6000+i;
            spawnServer(portToStart);
        }
        this.activeNoOfWorkers = i-1;
    }

    /**
     * Creates a new instance of server if {@link #activeNoOfWorkers} are less than {@link #MAX_NO_OF_WORKERS}
     */
    private void scaleUpServers() {
        if (this.activeNoOfWorkers < this.MAX_NO_OF_WORKERS) {
            LOGGER.info("Scaling Up Server");
            ++this.activeNoOfWorkers;
            spawnServer(6000+this.activeNoOfWorkers);
        }
    }

    /**
     * Terminates a instance of server if {@link #activeNoOfWorkers} are greater than {@link #MIN_NO_OF_WORKERS}
     */
    private void scaleDownServers() {
        if (this.activeNoOfWorkers > this.MIN_NO_OF_WORKERS) {
            LOGGER.info("Scaling Down Server");
            --this.activeNoOfWorkers;
            stopServer(6000+(this.activeNoOfWorkers+1));
        }
    }

    private void spawnServer(int portToStart) {
        int weight = mockRandomServerWeight();
        Server server = new Server(portToStart, weight);
        activeWorkers.put(portToStart, server);
        server.start();
        LOGGER.info("Started Server on port: " + portToStart + " (weight"+weight+")");
    }

    private int mockRandomServerWeight() {
        double d = Math.random();
        if (d < 0.7) {
            return 1;
        }
        if (d < 0.8) {
            return 2;
        }
        if (d < 0.9) {
            return 3;
        }
        return 4;
    }

    private void stopServer(int portToClose) {
        Server server = activeWorkers.remove(portToClose);
        server.close();
        LOGGER.info("Server on port: " + portToClose + " stopped");
    }

    /**
     * Starts a TCP connection with clients to listen requests
     * @param socket
     * to establish the client connection
     */
    public void manageClient(Socket socket) {
        new ClientRequestReader(socket);
    }

    /**
     * Queues the client {@param request} to be assigned to server
     */
    private void queueRequest(Request request) {
        LOGGER.info("Forwarding Client request: " + request);
        requests.put(request.getUUID(), request);
        queue.offer(request.getUUID());
    }

    /**
     * Maps the response to the appropriate client
     * and initiates scaling by checking TAT of response
     * @param packet response by server
     * @throws ParseException
     * @throws IOException
     */
    private void mapResponse(HTTPPacket packet) throws ParseException, IOException {
        Request request = requests.remove(packet.getUUID());
        long turnAroundTime = System.currentTimeMillis() - request.getArrivalTime();
        OutputStream ostream = request.getSocket().getOutputStream();
        ostream.write(
                (packet.getResponse()+"\n").getBytes()
        );
        ostream.flush();
        LOGGER.info(" Request " + request.getUUID() + " - TAT: " + turnAroundTime + "ms");
        if (turnAroundTime > this.MAX_SCALE_TIME_PER_REQUEST) {
            scaleUpServers();
        } else if ( ((turnAroundTime*100)/this.MAX_SCALE_TIME_PER_REQUEST) < 76 ) {
            scaleDownServers();
        }
    }

    public static void main(String[] args) throws IOException {
        Properties prop = new Properties();
        String fileName = "/home/sahil/Desktop/gc/assignment/Server/src/main/java/server/load_balancer/config/balancer.conf";
        try (FileInputStream fis = new FileInputStream(fileName)) {
            prop.load(fis);
        } catch (IOException e) {
            LOGGER.severe(e.toString());
        }

        String minWorkersStr = prop.getProperty("min_no_of_workers");
        String maxWorkersStr = prop.getProperty("max_no_of_workers");
        String maxScaleTimePerRequestStr = prop.getProperty("max_scale_time_per_request");

        int minWorkers = minWorkersStr != null ? Integer.parseInt(minWorkersStr) : 1;
        int maxWorkers = maxWorkersStr != null ? Integer.parseInt(maxWorkersStr) : Runtime.getRuntime().availableProcessors();
        int maxScaleTimePerRequest = maxScaleTimePerRequestStr != null ? Integer.parseInt(maxScaleTimePerRequestStr) : 100;

        LoadBalancer loadBalancer = new LoadBalancer(minWorkers, maxWorkers, maxScaleTimePerRequest);

        System.out.println("Started to listen to clients");
        try(ServerSocket serverSocket = new ServerSocket(5000)){
            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("Accepted client");
                loadBalancer.manageClient(socket);
            }
        } catch (IOException e) {
            LOGGER.severe("Server Exception: " + e);
        }
    }

    /**
     * Reads the response sent by server for a request
     */
    private class ServerInputReader extends Thread {
        DatagramSocket socket;
        ServerInputReader(DatagramSocket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            try{
                while(true){
                    byte[] buffer = new byte[4096];
                    DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                    socket.receive(packet);

                    String responseStr = new String(buffer, 0, packet.getLength());
                    HTTPPacket response = new HTTPPacket(responseStr);

                    // Logging response
                    String responseType = StatusCodes.getCodeType(response.getStatusCode());
                    if (StatusCodes.SUCCESSFUL.equals(responseType)) {
                        LOGGER.info("Response From Server to load balancer " + packet.getPort() + ": " + responseStr);
                    } else if (StatusCodes.CLIENT_ERROR.equals(responseType)) {
                        LOGGER.info("Response From Server to load balancer " + packet.getPort() + ": " + responseStr);
                    } else if (StatusCodes.SERVER_ERROR.equals(responseType)) {
                        LOGGER.severe("Response From Server to load balancer " + packet.getPort() + ": " + responseStr);
                    } else {
                        LOGGER.severe("Response From Server to load balancer " + packet.getPort() + ": " + responseStr);
                    }

                    mapResponse(response);
                }
            } catch (IOException e) {
                LOGGER.severe(e.toString());
            } catch (ParseException e) {
                LOGGER.severe("Invalid Packet Format: " + e);
            }
        }
    }

    /**
     * Processes the requests spooled in the {@link #queue}
     */
    private class RequestConsumer extends Thread {
        @Override
        public void run() {
            UUID requestID;
            while (true) {
                try {
                    requestID = queue.take();
                    Request request = requests.get(requestID);
                    assignToServer(request);
                } catch (InterruptedException | IOException e) {
                    LOGGER.severe(e.toString());
                }
            }
        }
    }

    /**
     * Reads the client requests and enqueue
     */
    private class ClientRequestReader extends Thread {
        Socket socket;
        ClientRequestReader(Socket socket) {
            this.socket = socket;
            this.start();
        }

        @Override
        public void run() {
            try {
                BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                while(true) {
                    String inputPacket = input.readLine();
                    queueRequest(
                            new server.Request(
                                    socket,
                                    inputPacket,
                                    System.currentTimeMillis(),
                                    UUID.randomUUID()
                            )
                    );
                }
            } catch (IOException e) {
                LOGGER.severe(e.toString());
            }
        }
    }
}