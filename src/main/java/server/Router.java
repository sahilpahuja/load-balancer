package server;

import http.HTTPPacket;
import http.StatusCodes;

public class Router {
    Controller controller;
    Router() {
        this.controller = new Controller();

    }

    /**
     * @param requestStr combination of request id and route name
     * @return the response packet after processing the request
     */
    public String processRequest(String requestStr) {
        String[] request = requestStr.split(":");
        String UUID = request[0];
        String route = request[1];
        switch (route) {
            case "/api/v1/hello": {
                return this.controller.hello(UUID);
            }
            case "/api/fail": {
                return this.controller.serverError(UUID);
            }
            default: {
                return new HTTPPacket(StatusCodes.NOT_FOUND, "Not Found", UUID).toString();
            }
        }
    }
}
