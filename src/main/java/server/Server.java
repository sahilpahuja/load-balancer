package server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Random;

public class Server extends Thread {
    /**
     * Indicates status of current thread.
     * Will stop listening on the socket when set false
     */
    private volatile boolean status;
    private final Random random;
    /**
     * Indicates the capacity of server
     */
    private final int weightCapacity;
    /**
     * Port on which the server is running currently
     */
    int serverPort;
    Router router;
    public Server(int portToStart, int weightCapacity) {
        this.random = new Random();
        this.serverPort = portToStart;
        this.router = new Router();
        this.weightCapacity = weightCapacity;
    }

    public int getWeightCapacity() {
        return this.weightCapacity;
    }

    @Override
    public void run() {
        try {
            status = true;
            startListenerForLoadBalancer(this.serverPort);
        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }

    public void close() {
        status = false;
    }

    /**
     * @param port - The port on which the server will start
     * @throws IOException If unable to open streams of socket
     */
    void startListenerForLoadBalancer(int port) throws IOException {
        DatagramSocket socket = new DatagramSocket(port);
        while(status){
            // Reading request Packet
            byte[] buffer = new byte[4096];
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
            socket.receive(packet);
            String route = new String(buffer, 0, packet.getLength());

            // Creating response packet
            processPolling();
            String responsePacket = this.router.processRequest(route);

            // Sending response packet
            byte[] buffer2 = responsePacket.getBytes();
            InetAddress senderAddress = packet.getAddress();
            int senderPort = packet.getPort();
            packet = new DatagramPacket(buffer2, buffer2.length, senderAddress, senderPort);
            socket.send(packet);
        }
        socket.close();
    }


    /**
     * Generate a random processing time for mocking
     * Considering the weight load of server
     */
    private void processPolling() {
        try {
            Thread.sleep(getRandomRange()/this.weightCapacity);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private int getRandomRange() {
        return this.random.nextInt((500 - 350) + 1) + 350;
    }
}

/*
=========================================================================================================================================================

clientRequestsManager:
	- port
	- url
	- no of clients that will be created and will start sending requests

ClientRequest extends Thread:
	- port
	- url
	- startPing() : will be called by run method of thread and will start sending requests to server by creating a new socket in a loop
		- Socket socket : will be created in this method
		- socket.outputStream("request");
		- socket.inputStream will parse the data and will close the connection

=========================================================================================================================================================

LoadBalancer
	- Queue connections - will be one time only
	- N threads will run for N connections whenever a request comes it will be inserted into requests

	- Queue requests HM (<int requestID> = [clientSocket, arrivalTime])

	- Queue servers
	- mapRequest() - where round robin algorithm will be inplemented

	- DatagramSocket - for servers to communicate with load balancer
		|
		input Stream - requestID: response : mapAndSendResponse()

	- mapAndSendResponse()

=========================================================================================================================================================

Server (A UDPServer)
	- construct(int nginxPort, String request)
	- routeParser(request)
	- Controller [class]
	- Response response = controller.getResponse(request)

=========================================================================================================================================================




client program:
input: no of clients which will request the server on correct endpoint and no. of client which will request the server on error endpoint.

info:
N different client sockets will start running.


LoadBalancer:
Will start on a specific port.
input:
no_of_workers

Info:
It will spawn/start no_of_worker servers each on diff Port
Server will store the sockets of all the servers

Programming Info:
In terms of java a SocketServer will start accepting clients request and will map these clients on a specific port.


Servers:
Info:
There will be multiple(no_of_workers) servers spawned by the load balancer.

This application server will process the endpoint and according to the endpoint the server will serve the request with delay of 350ms - 500ms




Create Min Number Of Servers

Max Num Of Servers

WAITING_TIME_THRESHOLD = 1000


ServersAvailableQueue
1
2
3
4



consume the request from RequestsQueue

assignToServer()
assign the request to available servers
wait until the server is free
if
(waitingTime reaches WAITING_TIME_THRESHOLD && currentNumberOfWorkers < MAX_NUM_OF_WORKERS) :
	spawnNewServerInstance()

ServersAvailableQueue
3
1
2
4

 */