package http;


/**
 * This Interface Contains http status codes
 */
public interface StatusCodes {
    /**
     * Response Types
     */
    String INFORMATIONAL = "informational";
    String SUCCESSFUL = "successful";
    String REDIRECTION = "redirection";
    String CLIENT_ERROR = "client_error";
    String SERVER_ERROR = "server_error";

    /**
     * Successful responses
     */
    int OK = 200;
    int CREATED = 201;
    int ACCEPTED = 202;

    /**
     * Client error responses
     */
    int BAD_REQUEST = 400;
    int UNAUTHORIZED = 401;
    int PAYMENT_REQUIRED = 402;
    int FORBIDDEN = 403;
    int NOT_FOUND = 404;
    int METHOD_NOT_ALLOWED = 405;
    int NOT_ACCEPTABLE = 406;
    int PROXY_AUTHENTICATION_REQUIRED = 407;
    int REQUEST_TIMEOUT = 408;
    int CONFLICT = 409;
    int GONE = 410;

    /**
     * Server error responses
     */
    int INTERNAL_SERVER_ERROR = 500;
    int NOT_IMPLEMENTED = 501;
    int BAD_GATEWAY = 502;
    int SERVICE_UNAVAILABLE = 503;
    int GATEWAY_TIMEOUT = 504;

    static String getCodeType(int code) {
        if (code >= 100 && code <= 199) {
            return INFORMATIONAL;
        }
        if (code >= 200 && code <= 299) {
            return SUCCESSFUL;
        }
        if (code >= 300 && code <= 399) {
            return REDIRECTION;
        }
        if (code >= 400 && code <= 499) {
            return CLIENT_ERROR;
        }
        if (code >= 500 && code <= 599) {
            return SERVER_ERROR;
        }
        return null;
    }
}
