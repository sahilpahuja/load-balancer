package http;

import org.json.simple.parser.ParseException;
import util.Sson;

import java.util.UUID;

public class HTTPPacket {
    private final Sson sson;

    public HTTPPacket(String packet) throws ParseException {
        this.sson = new Sson(packet);
    }

    public HTTPPacket(int statusCode, String response, String UUID) {
        try {
            this.sson = new Sson("{" +
                                        "\"headers\":{" +
                                            "\"general\":{" +
                                                "\"status_code\":" + "\"" + statusCode + "\"" +
                                            "}" +
                                            "\"response\":{" +
                                                "\"UUID\":" + "\"" + UUID + "\"" +
                                            "}" +
                                        "}," +
                                        "\"body\":{" +
                                            "\"message\":" + "\"" + response + "\"" +
                                        "}" +
                                    "}"
                                );
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public int getStatusCode() throws ParseException {
        return Integer.parseInt((String) this.sson.get("headers.general.status_code"));
    }

    public UUID getUUID() throws ParseException {
        return UUID.fromString((String) this.sson.get("headers.response.UUID"));
    }

    public String getResponse() throws ParseException {
        return (String) this.sson.get("body.message");
    }

    // Driver
    public static void main(String[] args) throws ParseException {
//        HTTPPacket parser = new HTTPPacket("{\"headers\":{\"general\":{\"status_code\":\"200\"}}}");
        HTTPPacket parser = new HTTPPacket(200, "Success", "ABCD");
        System.out.println(parser.getResponse());
    }

    @Override
    public String toString() {
        return sson.toString();
    }
}
