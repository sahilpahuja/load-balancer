package util;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * A custom class extending the functionality of {@link JSONObject}
 */
public class Sson {
    private final JSONParser parser;
    private final JSONObject jsonObject;
    public Sson(String s) throws ParseException {
        this.parser = new JSONParser();
        this.jsonObject = (JSONObject) this.parser.parse(s);
    }

    /**
     * @param key
     * Key indicates a string having JSON keys chained with dot(.) operator
     * @return string indicating JSON value
     * @throws ParseException
     * when wrong keys are specified
     */
    public Object get(String key) throws ParseException {
        String[] keys = key.split("[.]");
        String value = jsonObject.get(keys[0]).toString();
        for (int i = 1; i < keys.length; ++i) {
            value = ((JSONObject) this.parser.parse(value)).get(keys[i]).toString();
        }
        return value;
    }

    @Override
    public String toString() {
        return jsonObject.toString();
    }
}
